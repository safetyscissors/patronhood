namespace :works do
  desc "Start approved campaigns"
  task :start_campaigns => :environment do
    puts 'Starting campaigns...'

    works = Work.where(:status => WORK_APPROVED)

    if works.empty?
      puts "No campaigns today."
    else
      puts "Starting #{works.count} campaigns."
      works.each { | w | w.start_campaign }
    end
  end

  desc "End finished campaigns"
  task :end_campaigns => :environment do
    puts 'Ending campaigns...'

    works = Work.where("end_time < ? AND status = ?", 
                       DateTime.now, WORK_RUNNING)

    if works.empty?
      puts "No campaigns ending today."
    else  
      puts "#{works.count} campaigns ending today."
      works.each {| w | w.end_campaign }
    end   
  end
end
