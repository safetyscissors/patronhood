Patronhood::Application.routes.draw do
  root to: 'static_pages#home'

  # Devise Routes
  devise_for :users, :path_names => { 
                        :sign_in => "login", 
                        :sign_out => "logout", 
                        :sign_up => "signup" 
                      }, 
                     :controllers => {
                        :registrations => 'custom_devise/registrations'
                     }

  devise_scope :user do 
    match "signup" => "custom_devise/registrations#new"
    match "login" => "devise/sessions#new"
    delete "logout" => "devise/sessions#destroy"
  end

  # Beta User Routes
  resources :beta_users, :except => [:index, :show, :update, :edit]
  match "beta_signup" => "beta_users#new", :as => :beta_signup
  match '/beta_confirmation' => 'beta_users#confirmation'

  # Static Routes
  match '/dashboard', to: 'dashboard#index'
  match '/about', to: "static_pages#about"
  match '/faq', to: "static_pages#faq"
  match '/terms', to: "static_pages#terms"
  match '/team', to: "static_pages#team"
  match '/privacy', to: "static_pages#privacy"

  # Work Routes
  resources :works, :except => :index
  match '/new_work', to: 'works#new'
  match 'works/:id/cancel' => 'works#cancel', :as => 'cancel_work'
  match 'works/:id/approve' => 'works#approve', :as => 'approve_work'

  # Patronage Routes
  resources :works, :except => [:index] do
    resources :patrons, :except => [:index, :show, :update, :edit], :path_names => { :new => "fund" }
    resources :work_updates, :as => 'updates', :except => [:index, :show, :update, :edit]
  end

  match '/patronage_confirmation' => 'patrons#confirmation'

  unless Rails.application.config.consider_all_requests_local
    match '*a', to: 'errors#error_404'
  end


  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
