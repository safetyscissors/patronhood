module WorksHelper

  # Work submission check for models
  def submitting_work?
    submit
  end

  # Toggle work submission
  def submitting_work(work)
    work.submit = true
    work.location.submit = true
    work.detail.submit = true
    work.creator.submit = true
    work.media.submit = true

    work.rewards.each do | reward |
      reward.submit = true;
    end 

    work.media.renderings.each do | rendering |
      rendering.submit = true;
    end
  end

  def revert_submission(work)
    work.submit = false
    work.location.submit = false
    work.detail.submit = false
    work.creator.submit = false
    work.media.submit = false

    work.rewards.each do | reward |
      reward.submit = false;
    end 

    work.media.renderings.each do | rendering |
      rendering.submit = false;
    end
  end

  # Work Error Output
  def display_errors(work, attribute)
    output = ""
    unless work.nil?
      @work.errors[attribute].each do | error | 
        output += "<div class=\"error-field\">#{error}</div>\n"
      end

      return output.html_safe
    end
  end

end
