$(document).ready(function(){

  ////////////////////////////////////////////////////////////////////////////////////////////////// 
  // Subnav menu easing functions

  $('#menu a').bind('click',function(event){
    var $anchor = $(this);
    $('html, body').stop().animate({
      scrollTop: $($anchor.attr('href')).offset().top
    }, 1500,'easeInOutExpo');
    event.preventDefault();
  }); 

  $('#work-name a').bind('click',function(event){
    var $anchor = $(this);
    $('html, body').stop().animate({
      scrollTop: $($anchor.attr('href')).offset().top - 60.0
    }, 1500,'easeInOutExpo');
    event.preventDefault();
  }); 

  ////////////////////////////////////////////////////////////////////////////////////////////////// 
  // Work Save Action
  
  $('#savemywork').click(function() {
    var input = $("<input>").attr("type", "hidden").attr("name", "commit").val("Save My Work");
    $('#new_work').append($(input));
    $('#new_work')[0].submit(function(){ //listen for submit event
      alert('Handler for .submit() called.');
      return false;
    });
  });

  ////////////////////////////////////////////////////////////////////////////////////////////////// 
  // Work Save Change Action

  $('#savechanges').click(function() {
    var input = $("<input>").attr("type", "hidden").attr("name", "commit").val("Save Work Changes");
    $('.submit-work').append($(input));
    $('.submit-work')[0].submit(function(){ //listen for submit event
      alert('Handler for .submit() called.');
      return false;
    });
  });
  
  // Form Field Validation
  $('#field_with_errors').children().addClass(".field_error");

  ////////////////////////////////////////////////////////////////////////////////////////////////// 
  // Form Field Counter

  $.fn.counter = function() {
    return this.each(function() {
      var max_length = parseInt($(this).attr('maxlength'));

      var length = $(this).val().length;               
      $(this).parent().find('.counter_label').html(max_length-length + ' characters left');

      // bind on key up event
      $(this).keyup(function(){
        // calc length and truncate if needed
        var new_length = $(this).val().length;
        // update visual counter
        $(this).parent().find('.counter_label').html(max_length-new_length + ' characters left');
      });
    });
  };

  $('textarea[maxlength]').counter();

  //////////////////////////////////////////////////////////////////////////////////////////////////
  // Net Funding Calculator - Determine the net funding for a work

  $("input[name = 'work[funding_goal]']").keyup(function(){
    var total_val = $("input[name = 'work[funding_goal]']").val() * 0.9;

    if (total_val != NaN)
      $('#net_funding').html("Net amount when funded: $" + total_val.toFixed(2));
  });

  //////////////////////////////////////////////////////////////////////////////////////////////////
  // Works gallery tabs
  $('#myTab a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
  });

  $('#myTab a:first').tab('show');


  //////////////////////////////////////////////////////////////////////////////////////////////////
  // Setup carousel for work view

  $('.carousel').carousel('pause');


});