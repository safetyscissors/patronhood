$(document).ready(function(){
  /////////////////////////////////////////////////////////////////////////////
  // Initialise Stripe

  Stripe.setPublishableKey($('meta[name="stripe-key"]').attr('content'));

  /////////////////////////////////////////////////////////////////////////////
  // Initialise numeric only input for fields

  $(".positive-integer").numeric({ decimal: false, negative: false }, function() { alert("Positive integers only"); this.value = ""; this.focus(); });

  /////////////////////////////////////////////////////////////////////////////
  // Patronage Form - Determine which item is being selected && Total Funding

  $(".reward-select-section table tbody tr").unbind('click').click(function(){
    console.log($("#instructions").html());

    if ($(this).attr('value') == 0) {
      $("#status-optional").attr("id","status-required").html("Required");
      $("#instructions").html("You must specify an additional contribution amount below");
    } else {
      $("#status-required").attr("id","status-optional").html("Optional");
      $("#instructions").html("If you want to add more to your contribution, please enter it in the field below.");
    }
    
    var base_funding = $(this).find(".contribution").html();
    base_funding = base_funding.replace('$', '').replace(/\s/g, '');

    var additional_funding = $(".additional-values").find("input[name = 'patron[additional_contribution]']").val();

    calculate_total(base_funding, additional_funding);
    
    base_funding = base_funding.replace('$', '').replace(/\s/g, '');
    $('.selected-keepsake').find('.item').html($(this).find("h4").html());
    $(".reward-select-section table tbody").find(".selected").remove();
    $(this).find('#selected-marker').append('<div class="selected"></div>');

    var selected_item_id = "#patron_reward_selection_" + $(this).attr('value')
    console.log(selected_item_id);
    $(".selected-keepsake-input").find(selected_item_id).prop('checked', true);
  });

  /////////////////////////////////////////////////////////////////////////////
  // Calculate the total from the additional contribution input

  $("#additional-contribution").keyup(function() {
    if ($(this).val() == '0') {
        $(this).val('');
    }
    else
    {    
      var selected_item = $(".selected-keepsake-input").find(":checked").val();
      var selected_item_name = $(".reward-select-section table tbody tr[" + "value = '" + selected_item + "']").find("h4").html();
      
      var base_funding = $(".reward-select-section table tbody tr[" + "value = '" + selected_item + "']").find(".contribution").html();
      base_funding = base_funding.replace('$', '').replace(/\s/g, '');
      base_funding = Math.abs(base_funding);
      
      var additional_funding = $(".additional-values").find("input[name = 'patron[additional_contribution]']").val().replace(/^0+/, '');
      additional_funding = additional_funding.replace('$', '').replace(/\s/g, '');
      additional_funding = additional_funding.replace(/^0+/, '');
      additional_funding = Math.abs(additional_funding);

      calculate_total(base_funding, additional_funding);
    }  
  });
  
  /////////////////////////////////////////////////////////////////////////////
  // Contribution Overview Modal View Action

  $('#confirm-contribution').click(function(){
    // Personal Detals
    var first_name = $('.container-patronize .new_patron').find("#patron_first_name").val();
    var surname = $('.container-patronize .new_patron').find("#patron_surname").val();
    var email = $('.container-patronize .new_patron').find("#patron_email").val();

    // Address Details
    var address = $('.container-patronize .new_patron').find("#patron_address").val();
    var city = $('.container-patronize .new_patron').find("#patron_address_city").val();
    var zip = $('.container-patronize .new_patron').find("#patron_address_zip").val()
    var state = $('.container-patronize .new_patron').find("#patron_address_state").val();
    var country = $('.container-patronize .new_patron').find("#patron_address_country").val();
    var full_address = address + ",<br/>" + city + ",<br/>" + state + "<br/>" + country + " " + zip

    // Funding Details
    var additional_funding = $(".additional-values").find("input[name = 'patron[additional_contribution]']").val();
    additional_funding = additional_funding.replace('$', '').replace(/\s/g, '');
    additional_funding = additional_funding.replace(/^0+/, '');
    additional_funding = Math.abs(additional_funding);

    var selected_item = $(".selected-keepsake-input").find(":checked").val();
    var selected_item_name = $(".reward-select-section table tbody tr[" + "value = '" + selected_item + "']").find("h4").html();

    var base_funding = $(".reward-select-section table tbody tr[" + "value = '" + selected_item + "']").find(".contribution").html();
    base_funding = base_funding.replace('$', '').replace(/\s/g, '');
    base_funding = Math.abs(base_funding);

    var total_funding = parseInt(additional_funding) + parseInt(base_funding);

    // Determine the Funding Total
    $(".breakdown #keepsake").find(".value").html("$" + base_funding);
    $(".breakdown #keepsake").find(".desc").html(selected_item_name);
    $(".breakdown #additional").find(".value").html("$" + additional_funding);
    $(".breakdown .total").find("#total").html("$" + total_funding);

    // Determine the font size for funding values
    if (base_funding.length >= 3) {
      $(".breakdown #keepsake").find(".value").css("font-size", "35px");
    }

    if (additional_funding.length >= 3) {
      $(".breakdown #additional").find(".value").css("font-size", "35px");
    }

    // Format all other details
    $("#patronage-confirmation").find("#name").html(first_name + " " + surname);
    $("#patronage-confirmation").find("#email").html(email);
    $("#patronage-confirmation").find("#address").html(full_address);

    var validPersonalFields = patronage.verifyFields(first_name, surname, email);
    var validAddressFields = patronage.verifyFields(address, city, zip, state, country);

    if (validAddressFields && validPersonalFields && (total_funding > 0)) {
      $("#patronage-confirmation").modal();  
    } else {
      $("#patronage-invalid-fields").modal();
    }
    
  });

  /////////////////////////////////////////////////////////////////////////////
  // Fund Work Action

  $("#fund-work").unbind('click').click(function() {
    console.log("Funding work ...");
    $('#fund-work').attr('disabled', true);
    patronage.processCard();
  });

  $('#additional-contribution').click(function() {
    if ($(this).val() == 0) {
      $(this).val('');
    }
  });

  $('#additional-contribution').blur(function() {
    if ($(this).val() == '') {
      $(this).val('0');
    }
  });

});

function calculate_total (base_contrib, added_contrib) {
  var total = parseInt(base_contrib) + parseInt(added_contrib);
  console.log("Base: " + base_contrib + " Added: " + added_contrib);
  if (!isNaN(total)) {
    $("#total-calc").html("");
    $("#total-calc").html("$" + total);
  }
}

var patronage = {
  processCard: function() {
    var card;
    card = {
      number: $('#card_number').val(),
      cvc: $('#card_code').val(),
      expMonth: $('#card_month').val(),
      expYear: $('#card_year').val()
    };
    return Stripe.createToken(card, patronage.handleStripeResponse);
  },

  handleStripeResponse: function(status, response) {
    console.log (status);
    if (status == 200) {
      console.log("Successful Transaction");
      $('#patron_stripe_card_token').val(response.id);
      return $('#new_patron')[0].submit();
    } else {
      console.log("Failed Transaction");
      $("#stripe-error").addClass("alert alert-error");
      $('#stripe-error').text(response.error.message);
      return $('#fund-work').attr('disabled', false);
    }
  },

  verifyFields: function() {
    for (var i = 0; i < arguments.length; i++) {
      if (!arguments[i].length) {
        return false;
      }
    }
    return true;
  }
};
