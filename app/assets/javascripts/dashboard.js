$(document).ready(function(){
  // Dashboard Table Sorting
  $("#dashboard-works").tablesorter({ 
    headers: {
      4: {
        sorter: false
      }
    }   
  });

  // Dashboard tool-tip for actions
  $(".dashboard a").tooltip();

});