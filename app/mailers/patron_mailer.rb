class PatronMailer < ActionMailer::Base
  default from: "info@patronhood.com"
  
  def confirmation_email(patron, work)
    @patron = patron
    @work = work
    mail(:to => patron.email, 
         :subject => "Patronhood: Thank you for contributing to #{ work.name }")
  end

  def failed_campaign_email(patron, work)
    @patron = patron
    @work = work
    mail(:to => patron.email, 
         :subject => "Patronhood: Campaign Unsuccessful - #{ work.name }")
  end

  def successful_campaign_email(patron, work)
    @patron = patron
    @work = work
    mail(:to => patron.email, 
         :subject => "Patronhood: Campaign Successful! - #{ work.name }")
  end

  def cancelled_campaign_email(patron, work)
    @patron = patron
    @work = work
    mail(:to => patron.email, 
         :subject => "Patronhood: Campaign Cancelled - #{ work.name }")
  end

end
