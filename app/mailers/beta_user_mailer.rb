class BetaUserMailer < ActionMailer::Base
  default from: "info@patronhood.com"

  def activation_email(email, token)
    @email = email
    @token = token
    mail(:to => @email,
         :subject => "Welcome to the Patronhood Beta!")
  end
end
