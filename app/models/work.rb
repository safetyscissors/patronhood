# == Schema Information
#
# Table name: works
#
#  id           :integer         not null, primary key
#  name         :string(255)
#  summary      :string(350)
#  user_id      :integer
#  duration     :integer
#  created_at   :datetime        not null
#  updated_at   :datetime        not null
#  start_time   :datetime
#  end_time     :datetime
#  status       :integer
#  funding_goal :decimal(15, 2)  default(0.0)
#

include WorksHelper

class Work < ActiveRecord::Base
  attr_accessible :name, :funding_goal, :start_time, :end_time, :duration
  attr_accessible :summary, :location_attributes, :media_attributes
  attr_accessible :detail_attributes, :creator_attributes
  attr_accessible :rewards_attributes, :renderings_attributes
  attr_accessible :patrons_attributes
  attr_accessor   :submit

  # associations
  has_one 	:location, :dependent => :destroy
  has_one 	:media, :dependent => :destroy
  has_one 	:detail, :dependent => :destroy
  has_one 	:creator, :dependent => :destroy
  has_many 	:work_updates, :dependent => :destroy
  has_many 	:rewards, :dependent => :destroy
  has_many 	:patrons, :dependent => :destroy

  belongs_to :user

  # nested attribute access
  accepts_nested_attributes_for :location, :media, :detail
  accepts_nested_attributes_for :creator, :rewards, :patrons
	
  # Default Validations
  validates_presence_of :name 
  validates_uniqueness_of :name 
  validates_length_of :name, :maximum => 50

  validates :funding_goal, :numericality => true, :allow_blank => true
  validates_numericality_of :funding_goal, :greater_than => 0, 
                            :if => :submitting_work?

  validates :duration,  :presence => true, 
                        :numericality => true

  validates_inclusion_of :duration, :in => [30, 45, 60]
  validates_inclusion_of :status, :in => [WORK_SAVED, WORK_SUBMITTED, 
                                          WORK_APPROVED, WORK_REJECTED,
                                          WORK_SUCCESSFUL, WORK_UNSUCCESSFUL, 
                                          WORK_CANCELLED, WORK_RUNNING]

  validates_presence_of :summary, :if => :submitting_work?
  validates_length_of :summary, :maximum => 300
  validates_associated :rewards

  # Total Raised for Project
  def total_raised
    self.patrons.sum(:total_contribution)
  end

  # Percentage Raised for Project
  def percentage_raised
    if self.funding_goal == 0
      return 0
    else
      (self.total_raised.to_f / self.funding_goal.to_f) * 100
    end
  end

  # Campaign Start
  def start_campaign
    self.start_time = Date.today
    self.end_time = Date.today + self.duration
    self.status = WORK_RUNNING
    save!
  end

  # Campaign End
  def end_campaign
    if self.total_raised >= self.funding_goal
      self.status = WORK_SUCCESSFUL
      charge_patrons
    else
      self.status = WORK_UNSUCCESSFUL
      cancel_patronages
    end
    save!
  end

  # Cancel Campaign
  def cancel_campaign
    self.status = WORK_CANCELLED
    cancel_work
    save!
  end

  private
    def charge_patrons
      self.patrons.each do | p | 
        PatronMailer.successful_campaign_email(p, self).deliver
        p.charge
      end
    end

    def cancel_patronages
      self.patrons.each do | p | 
        PatronMailer.failed_campaign_email(p, self).deliver
        p.cancel_patronage 
      end
    end

    def cancel_work
      self.patrons.each do | p | 
        PatronMailer.cancelled_campaign_email(p, self).deliver
        p.cancel_patronage 
      end
    end

end
