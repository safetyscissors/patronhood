# == Schema Information
#
# Table name: details
#
#  id                  :integer         not null, primary key
#  description         :string(10000)
#  neighborhood_impact :string(10000)
#  work_id             :integer
#  created_at          :datetime        not null
#  updated_at          :datetime        not null
#

include WorksHelper

class Detail < ActiveRecord::Base
	attr_accessible :description, :neighborhood_impact
	attr_accessor :submit

	# associations
	belongs_to :work
	has_many :budget_items
		
	# valdiations
	validates_presence_of  :description, :neighborhood_impact,
	                                     :if => :submitting_work?

	validates_length_of   :description, :neighborhood_impact,
	                                    :maximum => 10000
	
end
