# == Schema Information
#
# Table name: work_updates
#
#  id          :integer         not null, primary key
#  description :string(2000)
#  work_id     :integer
#  created_at  :datetime        not null
#  updated_at  :datetime        not null
#

class WorkUpdate < ActiveRecord::Base
  attr_accessible :description

  # associations
  belongs_to :work

  # validations
  validates :work_id, :presence => true
end
