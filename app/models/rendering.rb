# == Schema Information
#
# Table name: renderings
#
#  id                 :integer         not null, primary key
#  media_id           :integer
#  title              :string(255)
#  created_at         :datetime        not null
#  updated_at         :datetime        not null
#  image_file_name    :string(255)
#  image_content_type :string(255)
#  image_file_size    :integer
#  image_updated_at   :datetime
#

include WorksHelper

class Rendering < ActiveRecord::Base
  attr_accessible  :image, :title

  has_attached_file :image,
    :styles => { 
      :thumb => "500x375", 
      :large => "700x575" 
    },
    :storage => :s3,
    :bucket => ENV['S3_BUCKET_NAME'],
    :s3_credentials => {
      :access_key_id => ENV['AWS_ACCESS_KEY_ID'],
      :secret_access_key => ENV['AWS_SECRET_ACCESS_KEY']
    },
    :s3_protocol => 'https'

  attr_accessor :submit

  # Associations
  belongs_to :media

  # Validation Checks
  validates :image, :attachment_presence => true,
                    :if => :submitting_work?
  validates_presence_of :title, :if => :submitting_work?
  validates_length_of :title, :maximum => 64
  validates_attachment_content_type :image, 
                                    :content_type => /^image\/(jpg|jpeg|pjpeg|png|x-png)$/, 
                                    :message => 'file type is not allowed (only jpeg/png images)'
end
