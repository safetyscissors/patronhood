# == Schema Information
#
# Table name: beta_users
#
#  id         :integer         not null, primary key
#  token      :string(255)
#  email      :string(255)
#  created_at :datetime        not null
#  updated_at :datetime        not null
#

class BetaUser < ActiveRecord::Base
  attr_accessible :email 

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, format: { with: VALID_EMAIL_REGEX }
  validates_uniqueness_of :email

  def activate 
    self.token = Digest::SHA1.hexdigest([Time.now, rand].join)
    #send email with token
    BetaUserMailer.activation_email(self.email, self.token).deliver
    save!
  end
end
