# == Schema Information
#
# Table name: creators
#
#  id                         :integer         not null, primary key
#  name                       :string(255)
#  bio                        :string(1000)
#  email                      :string(255)
#  website                    :string(255)
#  work_id                    :integer
#  created_at                 :datetime        not null
#  updated_at                 :datetime        not null
#  profile_image_file_name    :string(255)
#  profile_image_content_type :string(255)
#  profile_image_file_size    :integer
#  profile_image_updated_at   :datetime
#

include WorksHelper

class Creator < ActiveRecord::Base
  attr_accessible :name, :bio, :profile_image, :email, :website
  
  attr_accessor :submit

  has_attached_file :profile_image,
    :styles => { 
      :profile => "280x320" 
    },
    :storage => :s3,
    :bucket => ENV['S3_BUCKET_NAME'],
    :s3_credentials => {
      :access_key_id => ENV['AWS_ACCESS_KEY_ID'],
      :secret_access_key => ENV['AWS_SECRET_ACCESS_KEY']
    },
    :s3_protocol => 'https'
    
  # Associations
  belongs_to :work

  # Validations
  validates_presence_of :name, :bio, :profile_image, :email, :website,
                               :if => :submitting_work?
                               
  validates :profile_image, :attachment_presence => true,
                            :if => :submitting_work?

  validates_length_of :name, :maximum => 128
  validates_length_of :bio,  :maximum => 10000

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, format: { with: VALID_EMAIL_REGEX }, 
            :allow_blank => true
  validates :email, format: { with: VALID_EMAIL_REGEX }, 
            :allow_blank => false, :if => :submitting_work?

  validates_format_of :website, :with => URI::regexp(%w(http https)), 
                      :allow_blank => true
  validates_format_of :website, :with => URI::regexp(%w(http https)), 
                      :allow_blank => false, :if => :submitting_work?
  validates_attachment_content_type :profile_image, 
                                    :content_type => /^image\/(jpg|jpeg|pjpeg|png|x-png)$/, 
                                    :message => 'file type is not allowed (only jpeg/png images)'
end
