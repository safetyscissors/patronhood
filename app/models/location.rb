# == Schema Information
#
# Table name: locations
#
#  id           :integer         not null, primary key
#  latitude     :float
#  longitude    :float
#  neighborhood :string(255)
#  address      :string(255)
#  work_id      :integer
#  created_at   :datetime        not null
#  updated_at   :datetime        not null
#  city         :string(255)
#

include WorksHelper

class Location < ActiveRecord::Base
  before_validation :convert_location_data

  before_save :geocode, :if => :geocoded_by_address?
  before_save :reverse_geocode, :if => :geocoded_by_coordinates?

  geocoded_by :address, :if => :geocoded_by_address?
  reverse_geocoded_by :latitude, :longitude, :if => :geocoded_by_coordinates?

  attr_accessor :submit, :location_data

  attr_accessible :latitude, :longitude, :location_data
  attr_accessible :neighborhood, :city, :address

  # Associations
  belongs_to :work

  # Validations
  validates_presence_of :address, :neighborhood, :city,
                        :if => :submitting_work?

    def geocoded_by_coordinates?
      self.location_data =~ /^\s*-?\d+\.\d+\,\s?-?\d+\.\d+\s*$/ ? true : false
    end

    def geocoded_by_address?
      !geocoded_by_coordinates?
    end

    def convert_location_data 
      if !self.location_data.nil?
        if self.location_data =~ /^\s*-?\d+\.\d+\,\s?-?\d+\.\d+\s*$/
          self.latitude = self.location_data.split(',')[0].to_f
          self.longitude = self.location_data.split(',')[1].to_f
        else
          self.address = self.location_data  
        end
      end
    end

end
