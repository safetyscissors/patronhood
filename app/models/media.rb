# == Schema Information
#
# Table name: media
#
#  id                      :integer         not null, primary key
#  video_url               :string(255)
#  work_id                 :integer
#  created_at              :datetime        not null
#  updated_at              :datetime        not null
#  main_image_file_name    :string(255)
#  main_image_content_type :string(255)
#  main_image_file_size    :integer
#  main_image_updated_at   :datetime
#

include WorksHelper

class Media < ActiveRecord::Base
  attr_accessible :video_url, :main_image
  attr_accessible :renderings_attributes

  has_attached_file :main_image,
    :storage => :s3,
    :bucket => ENV['S3_BUCKET_NAME'],
    :styles => { 
      :thumb => "360x268" 
    },
    :s3_credentials => {
      :access_key_id => ENV['AWS_ACCESS_KEY_ID'],
      :secret_access_key => ENV['AWS_SECRET_ACCESS_KEY']
    },
    :s3_protocol => 'https'

  attr_accessor :submit	

  # associations
  has_many  :renderings, :dependent => :destroy
  belongs_to :work
  
  # nested attributes access
  accepts_nested_attributes_for :renderings

  # Conditional Validations
  validates :main_image, :attachment_presence => true,
                         :if => :submitting_work?

  VIDEO_URL_REGEX = /^.*youtube\.com\/watch\?v=\w*/
  validates :video_url, format: { with: VIDEO_URL_REGEX }, 
            :allow_blank => true
  validates :video_url, format: { with: VIDEO_URL_REGEX }, 
            :allow_blank => false, :if => :submitting_work?

  validates_attachment_content_type :main_image, 
                                    :content_type => /^image\/(jpg|jpeg|pjpeg|png|x-png)$/, 
                                    :message => 'file type is not allowed (only jpeg/png images)'

  def has_renderings? 
    unless self.renderings.empty?
      self.renderings.each do |r|
        return true unless r.image_file_name.nil?
      end
    end
    return false;
  end

end
