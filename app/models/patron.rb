# == Schema Information
#
# Table name: patrons
#
#  id                      :integer         not null, primary key
#  first_name              :string(255)
#  surname                 :string(255)
#  email                   :string(255)
#  address                 :string(255)
#  address_city            :string(255)
#  address_zip             :string(255)
#  address_state           :string(255)
#  address_country         :string(255)
#  reward_selection        :integer         default(0)
#  stripe_customer_token   :string(255)
#  base_contribution       :decimal(15, 2)  default(0.0)
#  additional_contribution :decimal(15, 2)  default(0.0)
#  total_contribution      :decimal(15, 2)  default(0.0)
#  work_id                 :integer
#  created_at              :datetime        not null
#  updated_at              :datetime        not null
#

class Patron < ActiveRecord::Base
  attr_accessible :first_name, :surname, :address, :address_city
  attr_accessible :address_zip, :address_state, :address_country
  attr_accessible :email, :reward_selection, :additional_contribution

  attr_accessible :stripe_card_token
  attr_accessor   :stripe_card_token

  # Associations
  belongs_to :work

  # Validations
  validates_presence_of :first_name, :surname, :address, :address_zip
  validates_presence_of :address_state, :address_city, :address_country
  validates_presence_of :email, :reward_selection, :work_id
  validates_presence_of :additional_contribution

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, format: { with: VALID_EMAIL_REGEX }
  validates_inclusion_of :reward_selection, 
                         :in => [REWARD_OPTION_NONE, REWARD_OPTION_1, 
                                 REWARD_OPTION_2, REWARD_OPTION_3]

  validates_numericality_of :address_zip
  validates_numericality_of :additional_contribution, 
                            :greater_than_or_equal_to => 0
  validates_numericality_of :total_contribution, 
                            :greater_than => 0
  validates_numericality_of :base_contribution, 
                            :greater_than_or_equal_to => 0

  # Model Functions
  ###################################################################

  def save_with_payment(reward)
    # Determine the reward option / tier by the user
    puts "Additional Contribution #{self.additional_contribution}"

    if reward.nil?
      self.base_contribution = 0
    else
      self.base_contribution = reward.minimum
    end
    
    # Calculate the total contribution
    self.total_contribution = self.base_contribution + 
                              self.additional_contribution

    if valid?
      puts "Stripe Card Token: #{stripe_card_token}"

      # Generate the Stripe customer token
      customer = Stripe::Customer.create(email: email, 
                                         card: stripe_card_token, 
                                         description: "{id:#{self.work.id} - #{reward.title} - 
                                                       #{self.first_name} #{self.surname}".tr("\n",""))
      self.stripe_customer_token = customer.id
      save!
    end
  rescue Stripe::InvalidRequestError => e
    logger.error "Stripe error while creating cutomer: #{e.message}"
    errors.add :base, e.message.split(': ')[0]
    false
  rescue Stripe::CardError => e
    logger.error "Stripe error while processing card: #{e.message}"
    errors.add :base, e.message.split(': ')[0]
    false
  end

  def charge
    # Convert contribution to cent format
    patron_contribution = self.total_contribution.to_i * 100
    Stripe::Charge.create(:amount => patron_contribution,
                          :currency => "usd",
                          :customer => self.stripe_customer_token,
                          :description => "Charge for #{self.first_name} #{self.surname}, 
                                          (#{self.email}) with #{self.total_contribution}".tr("\n",''))
  end

  def cancel_patronage
    self.stripe_customer_token = nil
    self.save!
  end

end
