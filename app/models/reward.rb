# == Schema Information
#
# Table name: rewards
#
#  id                 :integer         not null, primary key
#  description        :string(350)
#  quantity           :integer         default(0)
#  work_id            :integer
#  created_at         :datetime        not null
#  updated_at         :datetime        not null
#  title              :string(255)
#  image_file_name    :string(255)
#  image_content_type :string(255)
#  image_file_size    :integer
#  image_updated_at   :datetime
#  minimum            :decimal(15, 2)  default(0.0)
#

include WorksHelper

class Reward < ActiveRecord::Base
  attr_accessible :title, :description, :minimum, :quantity
  attr_accessor :submit

  # Associations
  belongs_to :work

  # Validation Checks
  validates_numericality_of :minimum, :greater_than => 0, :if => :submitting_work?
  validates_numericality_of :quantity, :allow_nil => true
  validates_presence_of :title, :description, :minimum,
                        :if => :submitting_work?

  validates_length_of :description, :maximum => 250
end
