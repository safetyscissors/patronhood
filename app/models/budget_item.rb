# == Schema Information
#
# Table name: budget_items
#
#  id          :integer         not null, primary key
#  title       :string(255)
#  value       :float
#  description :string(255)
#  detail_id   :integer
#  created_at  :datetime        not null
#  updated_at  :datetime        not null
#

class BudgetItem < ActiveRecord::Base
  attr_accessible :title, :description, :value	

  # Associations
  #belongs_to :detail

  # Validation
  validates :title, :presence => true
  validates :description, :presence => true
  validates :value, :presence => true, 
            :numericality => { :greater_than => 0 }

end
