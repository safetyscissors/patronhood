class WorksController < ApplicationController
  before_filter :authenticate_user!, :only => [:new, :create, :edit, :update, 
                                               :destroy, :approve, :cancel]
  before_filter :check_work_status_and_ownership, :only => [:show]
  before_filter :current_user_or_admin, :only => [:destroy, :edit, :cancel]
  before_filter :current_user_is_admin, :only => [:approve, :reject]
  
  def show
    @work = Work.find_by_id(params[:id])
    redirect_to root_path if @work.nil?
  end

  def new
    @work = current_user.works.new
    @work.build_location
    @work.build_media
    @work.build_detail
    @work.build_creator

    3.times { @work.rewards.build }
    @work.rewards = @work.rewards.sort
    
    7.times { @work.media.renderings.build }
  end

  def create    
    @work = current_user.works.new(params[:work])

    if params[:commit] == "Save My Work"
      @work.status = WORK_SAVED
      if @work.save! 
        flash[:notice] = "#{@work.name} was saved."   
        redirect_to edit_work_path(@work)
      else
        flash[:notice] = "An error occured"   
        render :new
      end
    elsif params[:commit] == "I'm Done! Submit My Work"
      submitting_work(@work)
      @work.status = WORK_SUBMITTED

      if @work.save
        flash[:notice] = "#{@work.name} has been submitted. The Patronhood 
                          team will look over your submission and get in 
                          contact with you if there are any problems or 
                          if things need clarification"
        redirect_to dashboard_path
      else
        revert_submission(@work)
        @work.status = WORK_SAVED
        if @work.save
          flash[:notice] = "#{@work.name} was not submitted because it was
                            lacking some information. For the meantime, you 
                            work has been saved and you can edit it anytime"
          redirect_to edit_work_path(@work)
        else
          flash[:notice] = "Whoops! Looks like you were trying to submit a 
                            work without any information! Please try again"
          render :new
        end
      end
    else
      render :new
    end
  end


  def edit 
    @work = Work.find_by_id(params[:id])
    @work.rewards = @work.rewards.sort
    
    if @work.status.eql?(WORK_RUNNING)
      flash[:notice] = "#{@work.name} campaign is currently running. 
                        You are unable to modify any of the projects 
                        details while in this state"
      redirect_to dashboard_path
    end
  end


  def update
    @work = current_user.works.find_by_id(params[:id])

    if params[:commit] == "Save Work Changes"
      @work.status = WORK_SAVED
      if @work.update_attributes(params[:work])
        flash[:notice] = "#{@work.name} was saved."   
        redirect_to edit_work_path(@work)
      else
        redirect_to edit_work_path(@work)
      end
    elsif params[:commit] == "I'm Done! Submit My Work"
      submitting_work(@work)
      @work.status = WORK_SUBMITTED
      if @work.update_attributes(params[:work])
        redirect_to dashboard_path
      else
        @work.status = WORK_SAVED
        render 'edit'
      end
    end
  end


  def destroy
    Work.find_by_id(params[:id]).destroy
    redirect_to dashboard_path
  end

  # Administrative Actions
  #####################################################

  def cancel
    @work = Work.find_by_id(params[:id])
    @work.status = WORK_CANCELLED

    if @work.cancel_campaign
      flash[:notice] = "#{@work.name} was cancelled."   
    else
      flash[:notice] = "There was a problem cancelling #{@work.name}. 
                        Please contact team@patronhood.com for assistance"   
    end
    
    redirect_to dashboard_path
  end

  def approve
    @work = Work.find_by_id(params[:id])
    @work.status = WORK_APPROVED

    if @work.save!
      flash[:notice] = "#{@work.name} was approved."   
    else
      flash[:notice] = "There was a problem approving #{@work.name}. 
                        Please contact team@patronhood.com for assistance"   
    end

    redirect_to dashboard_path
  end

  def reject
    @work = Work.find_by_id(params[:id])
    @work.status = WORK_REJECTED

    if @work.save!
      flash[:notice] = "#{@work.name} was rejected."   
    else
      flash[:notice] = "There was a problem rejecting #{@work.name}. 
                        Please contact team@patronhood.com for assistance."   
    end

    redirect_to dashboard_path
  end


  private

    # Before Filters
    #####################################################

    def check_work_status_and_ownership
      @work = Work.find_by_id(params[:id])
   
      unless @work.nil?
        if [WORK_SAVED, WORK_CANCELLED, WORK_SUBMITTED, WORK_REJECTED, WORK_APPROVED].include?(@work.status)
          if current_user.nil?
            redirect_to root_path
          elsif !current_user.works.include?(@work)
            redirect_to root_path unless current_user.admin?
          end
        end
      end
    end

    def current_user_or_admin
      @work = Work.find_by_id(params[:id])
      flash[:error] = "The work that you were trying to access does not exist" if @work.nil?
      redirect_to dashboard_path unless current_user.works.include?(@work) || current_user.admin?
    end

    def current_user_is_admin
      redirect_to dashboard_path unless current_user.admin?
    end
    
end
