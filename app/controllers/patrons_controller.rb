class PatronsController < ApplicationController
  before_filter :check_work_running, :only => [:new, :create]

  def new 
    @work = Work.find_by_id(params[:work_id])
    @work.rewards = @work.rewards.sort

    if !@work.nil? && @work.status.eql?(WORK_RUNNING)
        @patronage = @work.patrons.build
    else
      redirect_to @work 
    end

  end

  def create
    @work = Work.find_by_id(params[:work_id])
    @work.rewards = @work.rewards.sort
    
    if @work.nil?
      redirect_to root_path
    else
      @patronage = @work.patrons.build(params[:patron])
      reward_selection = params[:patron][:reward_selection].to_i
      
      if reward_selection > REWARD_OPTION_NONE
				@selected_reward = @work.rewards[reward_selection - 1]
	     	unless @selected_reward.quantity.nil?
	      	@selected_reward.quantity -= 1
      	end
      end
    
      if @patronage.save_with_payment(@selected_reward) && @work.save
        PatronMailer.confirmation_email(@patronage, @work).deliver
        redirect_to :action => :confirmation, 
                    :patron => @patronage.first_name, 
                    :work => @work.name
      else
        render :new 
      end
    end
  end

  def confirmation
    @work_name = params[:work]
    @patron_name = params[:patron]
  end

  private
    def check_work_running
      @work = Work.find_by_id(params[:work_id])
      redirect_to(root_path) unless @work.nil? || @work.status.eql?(WORK_RUNNING) 
    end

end
