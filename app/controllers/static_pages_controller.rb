class StaticPagesController < ApplicationController

  def home
    # List out all the projects
    @works = Work.where(:status => WORK_RUNNING)
  end

  def faq 
  end

  def about
  end

  def team
  end

  def terms
  end

  def privacy
  end

end
