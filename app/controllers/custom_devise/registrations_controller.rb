class CustomDevise::RegistrationsController < Devise::RegistrationsController
  
  def create
    email = params[:user][:email]
    beta_user = BetaUser.find_by_email(email)
  
    if User.find_by_email(email)
        redirect_to login_path
    else
      if beta_user.nil?
        # Cannot find beta user
        redirect_to new_beta_user_path
      else
        if beta_user.token.eql?(params[:registration_token]) && !beta_user.token.empty?
          # Beta user is invited
          super
        else
          # Beta user is not yet invited
          build_resource
          redirect_to new_beta_user_path
        end
      end
    end
  end

end
