class ApplicationController < ActionController::Base
  protect_from_forgery

  unless Rails.application.config.consider_all_requests_local
    #rescue_from Exception, :with => :render_error
    rescue_from ActiveRecord::RecordNotFound, :with => :render_not_found
    rescue_from AbstractController::ActionNotFound, :with => :render_not_found
    rescue_from ActionController::RoutingError, :with => :render_not_found
    rescue_from ActionController::UnknownController, :with => :render_not_found
    rescue_from ActionController::UnknownAction, :with => :render_not_found
  end 

  private

  def render_not_found(exception)
    Rails.logger.error(exception)
    render :template => "errors/error_404.html.haml", :layout => 'application', :status => 404
  end

  def render_error(exception)
    Rails.logger.error(exception)    
    render :template => "errors/error_500.html.haml", :layout => 'application', :status => 500
  end

  def redirect_error(exception)
    Rails.logger.error(exception)
    redirect_to root_path
  end

  def method_missing(m, *args, &block)
    Rails.logger.error(m)
    render :template => "errors/error_400.html.haml", :layout => 'application', :status => 400
    # or render/redirect_to somewhere else
  end
  
end
