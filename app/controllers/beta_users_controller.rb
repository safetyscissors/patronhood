class BetaUsersController < ApplicationController
  def new
    @beta_user = BetaUser.new
  end

  def create
    @beta_user = BetaUser.create(params[:beta_user])
    if @beta_user.save
      #redirect_to root_path 
      redirect_to :action => :confirmation
    else
      render :new
    end
  end

  def confirmation
  end

end
