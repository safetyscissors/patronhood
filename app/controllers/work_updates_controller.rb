class WorkUpdatesController < ApplicationController
  before_filter :authenticate_user!
  before_filter :current_user_owns_work

  def new
    @work = Work.find_by_id(params[:work_id])

    if !@work.nil? && @work.status.eql?(WORK_RUNNING)
        @work_updates = @work.work_updates.build
    else
      redirect_to dashboard_path
    end
  end

  def create
    @work = Work.find_by_id(params[:work_id])
    
    if @work.valid?
      @work_update = @work.work_updates.build(params[:work_update])
      if @work.save
        flash[:notice] = "#{@work.name} has been updated"
        redirect_to dashboard_path
      else
        flash[:error] = "#{@work.name} was not updated. Please try again."
        redirect_to dashboard_path
      end
    end  	
  end

  private
    def current_user_owns_work
      @work = Work.find_by_id(params[:work_id])
      redirect_to root_path unless current_user.works.include?(@work)
    end
end
