class DashboardController < ApplicationController
  before_filter :authenticate_user!

  def index
    @works = current_user.admin? ? Work.all : current_user.works.all
  end

end
