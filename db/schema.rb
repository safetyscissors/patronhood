# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120816033842) do

  create_table "beta_users", :force => true do |t|
    t.string   "token"
    t.string   "email"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "budget_items", :force => true do |t|
    t.string   "title"
    t.float    "value"
    t.string   "description"
    t.integer  "detail_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "creators", :force => true do |t|
    t.string   "name"
    t.string   "bio",                        :limit => 10000
    t.string   "email"
    t.string   "website"
    t.integer  "work_id"
    t.datetime "created_at",                                  :null => false
    t.datetime "updated_at",                                  :null => false
    t.string   "profile_image_file_name"
    t.string   "profile_image_content_type"
    t.integer  "profile_image_file_size"
    t.datetime "profile_image_updated_at"
  end

  create_table "details", :force => true do |t|
    t.string   "description",         :limit => 10000
    t.string   "neighborhood_impact", :limit => 10000
    t.integer  "work_id"
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
  end

  create_table "locations", :force => true do |t|
    t.float    "latitude"
    t.float    "longitude"
    t.string   "neighborhood"
    t.string   "address"
    t.integer  "work_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.string   "city"
  end

  create_table "media", :force => true do |t|
    t.string   "video_url"
    t.integer  "work_id"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
    t.string   "main_image_file_name"
    t.string   "main_image_content_type"
    t.integer  "main_image_file_size"
    t.datetime "main_image_updated_at"
  end

  create_table "patrons", :force => true do |t|
    t.string   "first_name"
    t.string   "surname"
    t.string   "email"
    t.string   "address"
    t.string   "address_city"
    t.string   "address_zip"
    t.string   "address_state"
    t.string   "address_country"
    t.integer  "reward_selection",        :default => 0
    t.string   "stripe_customer_token"
    t.decimal  "base_contribution"
    t.decimal  "additional_contribution"
    t.decimal  "total_contribution"
    t.integer  "work_id"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  create_table "renderings", :force => true do |t|
    t.integer  "media_id"
    t.string   "title"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "rewards", :force => true do |t|
    t.string   "description",        :limit => 350
    t.integer  "quantity"
    t.integer  "work_id"
    t.datetime "created_at",                                       :null => false
    t.datetime "updated_at",                                       :null => false
    t.string   "title"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "minimum",                           :default => 0
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "",    :null => false
    t.string   "encrypted_password",     :default => "",    :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.boolean  "admin",                  :default => false
  end

  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "work_updates", :force => true do |t|
    t.string   "description", :limit => 2000
    t.integer  "work_id"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
  end

  add_index "work_updates", ["work_id", "created_at"], :name => "index_work_updates_on_work_id_and_created_at"

  create_table "works", :force => true do |t|
    t.string   "name"
    t.string   "summary",      :limit => 350
    t.integer  "user_id"
    t.integer  "duration"
    t.datetime "created_at",                                 :null => false
    t.datetime "updated_at",                                 :null => false
    t.datetime "start_time"
    t.datetime "end_time"
    t.integer  "status"
    t.integer  "funding_goal",                :default => 0
  end

end
