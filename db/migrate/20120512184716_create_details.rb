class CreateDetails < ActiveRecord::Migration
  def change
    create_table :details do |t|	
			t.string	:description, :limit => 10000
			t.string  :neighborhood_impact, :limit => 10000

			t.integer :work_id

      t.timestamps
    end
  end
end
