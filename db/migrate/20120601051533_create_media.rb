class CreateMedia < ActiveRecord::Migration
  def change
    create_table :media do |t|
      t.string  :video_url
  
      t.integer	:work_id
        
      t.timestamps
    end
  end
end
