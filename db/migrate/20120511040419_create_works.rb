class CreateWorks < ActiveRecord::Migration
  def change
    create_table :works do |t|
      t.string	 :name
      t.string   :summary, :limit => 350
      t.integer  :user_id
      t.integer  :duration

      t.timestamps      
    end
  end
end
