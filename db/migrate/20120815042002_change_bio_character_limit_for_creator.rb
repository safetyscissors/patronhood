class ChangeBioCharacterLimitForCreator < ActiveRecord::Migration
  def change
	  change_column :creators, :bio, :string, :limit => 10000	
	end
end
