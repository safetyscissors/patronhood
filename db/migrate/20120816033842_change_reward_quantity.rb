class ChangeRewardQuantity < ActiveRecord::Migration
  def change
    change_column :rewards, :quantity, :integer, :null => true	
  end
end
