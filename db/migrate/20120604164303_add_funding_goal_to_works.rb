class AddFundingGoalToWorks < ActiveRecord::Migration
  def change
    add_column :works, :funding_goal, :integer, :default => 0
  end
end
