class CreateCreators < ActiveRecord::Migration
  def change
    create_table :creators do |t|
			t.string		:name
			t.string		:bio, :limit => 1000
			t.string    :email
			t.string    :website

			t.integer		:work_id

      t.timestamps
    end
  end
end
