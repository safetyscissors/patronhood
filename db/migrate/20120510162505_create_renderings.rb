class CreateRenderings < ActiveRecord::Migration
  def change
    create_table :renderings do |t|
      t.integer :media_id
      t.string  :title

      t.timestamps
    end
  end
end
