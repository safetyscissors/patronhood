class AddAttachmentImageToRewards < ActiveRecord::Migration
  def self.up
    add_column :rewards, :image_file_name, :string
    add_column :rewards, :image_content_type, :string
    add_column :rewards, :image_file_size, :integer
    add_column :rewards, :image_updated_at, :datetime
  end

  def self.down
    remove_column :rewards, :image_file_name
    remove_column :rewards, :image_content_type
    remove_column :rewards, :image_file_size
    remove_column :rewards, :image_updated_at
  end
end
