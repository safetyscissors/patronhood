class AddMinimumToRewards < ActiveRecord::Migration
  def change
    add_column :rewards, :minimum, :integer, :default => 0
  end
end
