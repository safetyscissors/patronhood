class CreateWorkUpdates < ActiveRecord::Migration
  def change
    create_table :work_updates do |t|
			t.string	:description, :limit => 2000

			t.integer	:work_id

      t.timestamps
    end
		add_index :work_updates, [:work_id, :created_at]
  end
end
