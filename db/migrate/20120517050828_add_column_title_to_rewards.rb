class AddColumnTitleToRewards < ActiveRecord::Migration
  def change
    add_column :rewards, :title, :string
  end
end
