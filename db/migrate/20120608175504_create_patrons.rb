class CreatePatrons < ActiveRecord::Migration
  def change
    create_table :patrons do |t|
      t.string    :first_name, :surname, :email, :address, :address_city
      t.string    :address_zip, :address_state, :address_country

      t.integer   :reward_selection, :default => 0
      t.string    :stripe_customer_token

      t.decimal   :base_contribution
      t.decimal   :additional_contribution
      t.decimal   :total_contribution

      t.integer   :work_id

      t.timestamps
    end
  end
end
