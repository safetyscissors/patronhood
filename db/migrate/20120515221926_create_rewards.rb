class CreateRewards < ActiveRecord::Migration
  def change
    create_table :rewards do |t|
      t.string  :description, :limit => 350
      t.integer :quantity

      t.integer	:work_id
      t.timestamps
    end
  end
end
