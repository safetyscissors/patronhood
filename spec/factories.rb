FactoryGirl.define do
	factory :work do
		name	"Awesome project!"
		funding_goal	5_000_000
		start_time 	"1-1-1984"
		end_time 	"31-1-1984"
		duration  30
	end

	factory :detail do
		description "Some text that was added here"
		work
	end

end
