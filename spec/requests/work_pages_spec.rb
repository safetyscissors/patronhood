require 'spec_helper'

describe "WorkPages" do
	subject { page }

  describe "new work page" do
		before { visit new_work_path }
	
		describe "save with invalid information" do
			it "should not create a new work" do
				expect { click_button 'Save Work' }.not_to change(Work, :count)
			end
		end

		describe "save with minimal information" do
			before do
				fill_in 'work_name',  with: "Hundered-Story House"
			end

			it "should create a work" do
				expect { click_button 'Save Work' }.to change(Work, :count).by(1)
			end
		end

    describe "submit with no information" do
      it "should not create a work" do
				expect { click_button 'Submit Work' }.not_to change(Work, :count)
      end
    end

    describe "submit with valid information" do
    end
  end
end
