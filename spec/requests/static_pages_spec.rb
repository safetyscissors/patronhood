require 'spec_helper'

describe "StaticPages" do
	describe "Home Page" do
		it "should have the content Patronhood" do
			visit root_path
			page.should have_content('Patronhood')
		end
	end

	describe "About Page" do
		it "should have the content 'About Patronhood'" do
			visit about_path
			page.should have_content('About Patronhood');
		end 
	end
end
