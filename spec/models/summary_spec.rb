# == Schema Information
#
# Table name: summaries
#
#  id          :integer         not null, primary key
#  description :string(255)
#  video_url   :string(255)
#  work_id     :integer
#  created_at  :datetime        not null
#  updated_at  :datetime        not null
#

require 'spec_helper'

describe Summary do
	let(:work) { FactoryGirl.create(:work) }
	before { @summary = work.build_summary(description: "A description", video_url: "www.vimeo.com") }

	subject { @summary }

	it { should respond_to(:description) }
	it { should respond_to(:video_url) }
	it { should respond_to(:work_id) }
	it { should respond_to(:work) }

	its(:work) { should == work }

	it { should be_valid }

	describe "accessible attributes" do
		it "should not allow access to work_id" do
			expect do
				Summary.new(work_id: work.id)
			end.should raise_error(ActiveModel::MassAssignmentSecurity::Error)
		end
	end

end
