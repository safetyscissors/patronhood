# == Schema Information
#
# Table name: works
#
#  id           :integer         not null, primary key
#  name         :string(255)
#  summary      :string(350)
#  user_id      :integer
#  duration     :integer
#  created_at   :datetime        not null
#  updated_at   :datetime        not null
#  start_time   :datetime
#  end_time     :datetime
#  status       :integer
#  funding_goal :decimal(15, 2)  default(0.0)
#

require 'spec_helper'

describe Work do
	before { @work = Work.new(name: "Awesome work", funding_goal: 500_000, duration: 60) }
	
	subject { @work }

	it { should respond_to(:name) }
	it { should respond_to(:funding_goal) }
	it { should respond_to(:start_time) }
	it { should respond_to(:end_time) }
	it { should respond_to(:duration) }

	# associations
	it { should respond_to(:detail)}
	it { should respond_to(:location)}
	it { should respond_to(:project_updates)}
	it { should respond_to(:summary) }
	it { should respond_to(:artist) }
	it { should respond_to(:rewards) }
	it { should respond_to(:renderings) }

	describe "when name is not present" do
		before { @work.name = nil }
		it { should_not be_valid }
	end

	describe "when work name is too long" do
		before { @work.name = 'a' * 300 }
		it { should_not be_valid }
	end

	describe "when funding goal is not the right format" do
		before { @work.funding_goal = "0xDEADBEEF" }
		it { should_not be_valid }
	end
	
	describe "when funding goal is greater than $100" do
		before { @work.funding_goal = 100.00 }
		it { should be_valid }
	end

  describe "when duration is not 30, 45 or 60" do
    before { @work.duration = 1000 }
    it { should_not be_valid }
  end 

end
