# == Schema Information
#
# Table name: renderings
#
#  id                 :integer         not null, primary key
#  media_id           :integer
#  title              :string(255)
#  created_at         :datetime        not null
#  updated_at         :datetime        not null
#  image_file_name    :string(255)
#  image_content_type :string(255)
#  image_file_size    :integer
#  image_updated_at   :datetime
#

require 'spec_helper'

describe Rendering do
let(:work) { FactoryGirl.create(:work) }
	before do
		@rendering = work.renderings.build();
	end

	subject { @rendering }

  it { should respond_to(:title) }
  it { should respond_to(:image) }


	its(:work) { should == work }
	it { should be_valid }

	describe "accessible attributes" do
		it "should not allow access to work_id" do
			expect do
				Location.new(work_id: work.id)
			end.should raise_error(ActiveModel::MassAssignmentSecurity::Error)
		end
	end

end
