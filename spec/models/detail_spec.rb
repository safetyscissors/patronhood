# == Schema Information
#
# Table name: details
#
#  id                  :integer         not null, primary key
#  description         :string(10000)
#  neighborhood_impact :string(10000)
#  work_id             :integer
#  created_at          :datetime        not null
#  updated_at          :datetime        not null
#

require 'spec_helper'

describe Detail do
let(:work) { FactoryGirl.create(:work) }
	before { @detail = work.build_detail(description: "something that was written here") }

	subject { @detail }

	it { should respond_to(:description) }
	it { should respond_to(:work_id) }
	it { should respond_to(:work) }

	its(:work) { should == work }

	it { should be_valid }

	describe "accessible attributes" do
		it "should not allow access to work_id" do
			expect do
				Location.new(work_id: work.id)
			end.should raise_error(ActiveModel::MassAssignmentSecurity::Error)
		end
	end

end
