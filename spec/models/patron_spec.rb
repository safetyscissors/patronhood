# == Schema Information
#
# Table name: patrons
#
#  id                      :integer         not null, primary key
#  first_name              :string(255)
#  surname                 :string(255)
#  email                   :string(255)
#  address                 :string(255)
#  address_city            :string(255)
#  address_zip             :string(255)
#  address_state           :string(255)
#  address_country         :string(255)
#  reward_selection        :integer         default(0)
#  stripe_customer_token   :string(255)
#  base_contribution       :decimal(15, 2)  default(0.0)
#  additional_contribution :decimal(15, 2)  default(0.0)
#  total_contribution      :decimal(15, 2)  default(0.0)
#  work_id                 :integer
#  created_at              :datetime        not null
#  updated_at              :datetime        not null
#

require 'spec_helper'

describe Patron do
  pending "add some examples to (or delete) #{__FILE__}"
end
