# == Schema Information
#
# Table name: rewards
#
#  id                 :integer         not null, primary key
#  description        :string(350)
#  quantity           :integer         default(0)
#  work_id            :integer
#  created_at         :datetime        not null
#  updated_at         :datetime        not null
#  title              :string(255)
#  image_file_name    :string(255)
#  image_content_type :string(255)
#  image_file_size    :integer
#  image_updated_at   :datetime
#  minimum            :decimal(15, 2)  default(0.0)
#

require 'spec_helper'

describe Reward do
	  let(:work) { FactoryGirl.create(:work) }
  before { @reward = work.rewards.build(description: "Picture of me", upper_limit: 5) }

  subject { @reward }

  it { should respond_to (:description) }
  it { should respond_to (:upper_limit) }
  it { should respond_to(:work) }

  its(:work) { should == work }

  it { should be_valid }

  describe "accessible attributes" do
    it "should not allow access to work_id" do
      expect do
        Reward.new(work_id: work.id)
      end.should raise_error(ActiveModel::MassAssignmentSecurity::Error)
    end
  end
end
