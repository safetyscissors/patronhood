# == Schema Information
#
# Table name: budget_items
#
#  id          :integer         not null, primary key
#  title       :string(255)
#  value       :float
#  description :string(255)
#  detail_id   :integer
#  created_at  :datetime        not null
#  updated_at  :datetime        not null
#

require 'spec_helper'

describe BudgetItem do
let(:detail) { FactoryGirl.create(:detail) }
	before do
		@budget_item = detail.budget_items.build(value: 500_000, description: "Beer", title: "Beer Budget")
	end

	subject { @budget_item }

	it { should respond_to(:value) }
	it { should respond_to(:description) }
	it { should respond_to(:title) }

	its(:detail) { should == detail }

	it { should be_valid }

	describe "accessible attributes" do
		it "should not allow access to work_id" do
			expect do
				Location.new(detail_id: detail.id)
			end.should raise_error(ActiveModel::MassAssignmentSecurity::Error)
		end
	end

	describe "when title is nil" do
    before { @budget_item.title = nil }
    it { should_not be_valid }
	end

	describe "when description is nil" do
    before { @budget_item.description = nil }
    it { should_not be_valid }
	end

	describe "when value is nil" do
    before { @budget_item.value = nil }
    it { should_not be_valid }
	end

	describe "when budget item value is 0" do
    before { @budget_item.value = 0 }
    it { should_not be_valid }
  end

	describe "when budget item value is 100" do
    before { @budget_item.value = 100 }
    it { should be_valid }
  end

end
