# == Schema Information
#
# Table name: artists
#
#  id                         :integer         not null, primary key
#  name                       :string(255)
#  bio                        :string(10000)
#  work_id                    :integer
#  created_at                 :datetime        not null
#  updated_at                 :datetime        not null
#  profile_image_file_name    :string(255)
#  profile_image_content_type :string(255)
#  profile_image_file_size    :integer
#  profile_image_updated_at   :datetime
#  organization               :string(255)
#

require 'spec_helper'

describe Artist do
	let(:work) { FactoryGirl.create(:work) }
	before { @artist = work.build_artist(name: "John Smith", bio: "Awesome person who likes stuff") }

	subject { @artist }

	it { should respond_to (:name) }
	it { should respond_to (:bio) }
	it { should respond_to (:work) }
	it { should respond_to (:profile_image) }
	it { should respond_to (:organization) }
	
	its(:work) { should == work }

	it { should be_valid } 

	describe "accessible attributes" do
  	it "should not allow access to work_id" do
    	expect do
     		Artist.new(work_id: work.id)
      end.should raise_error(ActiveModel::MassAssignmentSecurity::Error)
    end
  end
end
