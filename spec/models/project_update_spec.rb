# == Schema Information
#
# Table name: project_updates
#
#  id          :integer         not null, primary key
#  name        :string(255)
#  description :string(2000)
#  work_id     :integer
#  created_at  :datetime        not null
#  updated_at  :datetime        not null
#

require 'spec_helper'

describe ProjectUpdate do
	let(:work) { FactoryGirl.create(:work) }
	before { @update = work.project_updates.build(name: "Some Title", description: "A description") }

	subject { @update }

	it { should respond_to(:name) }
	it { should respond_to(:description) }
	it { should respond_to(:work_id) }
	it { should respond_to(:work) }

	its(:work) { should == work }

	it { should be_valid }

	describe "accessible attributes" do
		it "should not allow access to work_id" do
			expect do
				ProjectUpdate.new(work_id: work.id)
			end.should raise_error(ActiveModel::MassAssignmentSecurity::Error)
		end
	end

	describe "when work_id is not present" do
		before { @update.work_id = nil }
		it { should_not be_valid }
	end
end
