# == Schema Information
#
# Table name: locations
#
#  id           :integer         not null, primary key
#  latitude     :float
#  longitude    :float
#  neighborhood :string(255)
#  city         :string(255)
#  address      :string(255)
#  work_id      :integer
#  created_at   :datetime        not null
#  updated_at   :datetime        not null
#

require 'spec_helper'

describe Location do
	let(:work) { FactoryGirl.create(:work) }
	before { @location = work.build_location(latitude: -50.0, longitude: -50.0, 
                                           neighborhood: "Brooklyn", city: "New York") }

	subject { @location }

	it { should respond_to (:latitude) }
	it { should respond_to (:longitude) }
	it { should respond_to (:neighborhood) }
	it { should respond_to (:city) }
	it { should respond_to (:neighborhood) }

	it { should respond_to(:work) }

	its(:work) { should == work }

	it { should be_valid }

	describe "accessible attributes" do
		it "should not allow access to work_id" do
			expect do
				Location.new(work_id: work.id)
			end.should raise_error(ActiveModel::MassAssignmentSecurity::Error)
		end
	end

end
